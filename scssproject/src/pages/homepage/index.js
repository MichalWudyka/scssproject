import React from 'react';
import Header from '../../components/Header';
import About from '../../components/About';
import Projects from '../../components/Projects';
import Contact from '../../components/Contact';
import Footer from '../../components/Footer';

const Homepage = () => {

return(
  <div className="main-onePage__wrapper" >
    <Header/>
    <About/> 
    <Projects/> 
    <Contact/> 
    <Footer/>
    <div className='empty'></div>
  </div>
)

};

export default Homepage;


