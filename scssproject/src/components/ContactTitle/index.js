import BGcontact from '../../assets/BGcontact.png';
import BGelipsa from '../../assets/BGelipsa.png';
const ContactTitle = () => {

	return (
		<div className="contactTitle__main-wrapper">
			<img src={BGcontact} className=" contactTitle__BGround-img" alt="BGround-img"/>
			<img src={BGelipsa} className="contactTitle__elipsa-img" alt="elipsa-img"/>
			<span className="grayTextColor  contactTitle__text-style" id="contact">&#47;&#47; 
				<span className="pinkTextColor">Contact</span>
			</span>
		</div>	
	)
}

export default ContactTitle;
