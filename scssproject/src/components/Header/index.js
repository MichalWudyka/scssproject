import Navigation from '../Navigation';
import React, { useEffect , useState} from 'react';

import im from '../../assets/Im.png'; 
import logo from '../../assets/logo.png'; 
import wave1 from '../../assets/wave1.png'; 
import wave2 from '../../assets/wave2.png'; 
import wave3 from '../../assets/wave3.png'; 
import obj1 from '../../assets/obj1.png'; 
import obj2 from '../../assets/obj2.png'; 

const Header = () => {

const [elementsChanged, setElementsChanged]= useState(false);

  useEffect(() => {
    window.onscroll = () => {
    		document.getElementById("aboutMe").style.visibility = "visible";
      	document.getElementById("myProjectsButton").style.background = "#e56385";
      	document.getElementById("myProjectsButton").style.color = "#fff";


			if (elementsChanged === false) {

      	document.getElementById("aboutMe").animate([
 					{ transform: 'translateY(500px)' },
 					{ transform: 'translateY(0px)' }
					], {
  				duration: 1000,
  				iterations: 1
				});
      }
      setElementsChanged(true)
    }
  });

	return (
		<>
			<Navigation/>

			<div className="header__main-wrapper">
  			<img src={wave1}  className="backGround__wave1 objSize" alt="icon_wave1"/>
				<img src={wave2}  className="backGround__wave2 objSize" alt="icon_wave2"/>
				<img src={wave3}  className="backGround__wave3 objSize" alt="icon_wave3"/>
				<img src={obj1} 	className="backGround__obj1  objSize" alt="icon_obj1"/>
				<img src={obj2} 	className="backGround__obj2  objSize" alt="icon_obj2"/>
		
				<div className="header__content-wrapper">
					<div className="header__content-im">
						<div className="header__im-background">
							<img src={im} alt="I'm img"/>
						</div>
		
						<div className="header__content-information">
							<p className="grayTextColor header-textSize" >Front-end developer <span className="pinkTextColor">&#38;</span> Programmer</p>
							<p className="grayTextColor header-nameTextSize">MICHAŁ WUDYKA</p>
							<br/>
  		    		<a href="#projects" className="pinkBorderButton allButtons" id="myProjectsButton">My projects</a>
							<a href="#aboutMe" className="pinkTextColor allButtons">More about me</a>
						</div>
					</div>
				</div>

				<div className="header__photo-wrapper" id="photoWrapper">
					<div className="header__photo-border">
						<img src={logo} id="photo" alt="My_photo"/>
					</div>
				</div>
			</div>	
		</>
	)
}


export default Header;

	