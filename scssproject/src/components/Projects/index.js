const Projects = () => {

	return (
		<div className=" projectsTitle__main-wrapper" id="projects">
			<div className="projectsTitle-overlap" id="projects">
				<span className="grayTextColor projectsTitle__title-style">&#47;&#47; <span className="whiteTextColor">Projects</span></span>
			</div>
			<br/><br/>
			
			<div className="content__main-wrapper">
				<div className="projects__content-wrapper">
					<div className="projects__navigation-wrapper">
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">HTML</a>
						</div>
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">CSS</a>
						</div>
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">JAVA SCRIPT</a>
						</div>
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">REACT</a>
						</div>
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">LOREM</a>
						</div>
						<div className="projects__button-style">
							<a href="#projects" className="grayTextColor ">IPSUM</a>
						</div>
					</div>

					<div className="projects__boxes-wrapper">
						<div className="box">BOX</div>
						<div className="box">BOX</div>
						<div className="box">BOX</div>
						<div className="box last">BOX LAST</div>
					</div>	

				</div>
			</div>
		</div>
	)
}


export default Projects;




		



