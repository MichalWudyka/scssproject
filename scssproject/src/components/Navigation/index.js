import React from 'react';
import logo from '../../assets/logo.png'; 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faInstagram  } from '@fortawesome/free-brands-svg-icons';

const Navigation = () => {
const instaIcon = <FontAwesomeIcon icon={faInstagram} />;

	return (
		<div className="navigation__main-wrapper">
			<div className="content__main-wrapper">

				<div className="navigation__logo-wrapper">
					<img src={logo}  id="logo" className="logo" alt="My_logo"/>
				</div>

				<div className="navigation__buttons-wrapper" id="navDivSize">

					<div >
						<div className="navigation-buttons">
							<a href="#projects" className="pinkTextColor allButtons">Project</a>
						</div>
						<div className="navigation-buttons">
							<a href="#aboutMe" className="grayTextColor allButtons">About me</a>
						</div>
						<div className="navigation-buttons">
							<a href="#contact" className="navigation__contact-button allButtons">Contact</a>
						</div>
					</div>

					<div >
						<div className="navigation-buttons">
							<a href="#contact" className="pinkTextColor allButtons iconSize">f</a>
						</div>
						<div className="navigation-buttons">
							<a href="#contact" className="grayTextColor allButtons iconSize">{instaIcon}</a>
						</div>
						<div className="navigation-buttons">
							<a href="#contact" className="grayTextColor allButtons iconSize">in</a>
						</div>
					</div>

				</div>
			</div>
			<hr className="navigation__main-line"/>	
		</div>
	)
}


export default Navigation;