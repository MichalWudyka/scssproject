import BGroundImg from '../../assets/BGon.png';
import React, { useEffect } from 'react';

const About = () => {

	useEffect(() => {
   	function value(a,b,c,d,e,f){
	
		document.getElementById("skillsHTML-value").textContent = a + "%";
		document.getElementById("skillsCSS-value").textContent = b + "%";
		document.getElementById("skillsJS-value").textContent = c + "%";
		document.getElementById("skillsREACT-value").textContent = d + "%";
		document.getElementById("skillsHTML1-value").textContent = e + "%";
		document.getElementById("skillsHTML2-value").textContent = f + "%";
	
		document.getElementById("skillsHTML").style.width = (a/100) * 90 + "%";
		document.getElementById("skillsCSS").style.width = (b/100) * 90 + "%";
		document.getElementById("skillsJS").style.width = (c/100) * 90 + "%";
		document.getElementById("skillsREACT").style.width = (d/100) * 90 + "%";
		document.getElementById("skillsHTML1").style.width = (e/100) * 90 + "%";
		document.getElementById("skillsHTML2").style.width = (f/100) * 90 + "%";
	}
	value(98,90,75,50,80,60)
  });


	return (
		
	<div className="about__main-wrapper">
		<div className="content__main-wrapper">
			<div className="about__content-wrapper" id="aboutMe">
				<p className="about__url-path">&#47;&#47;About</p>
				<div className="about__inner-wrapper">  
					<span className="grayTextColor about__title-fontSize">&#47;&#47;</span>
					<span className="pinkTextColor about__title-fontSize">About</span>
					<div className="left-border flex-wrapper">
						<div className="aboutMe__content-wrapper">					
							<span className="grayTextColor about__title-fontSize2">&lt;&#47;</span>
							<span className="pinkTextColor about__title-fontSize2">Me</span>
							<span className="grayTextColor about__title-fontSize2">&gt;</span>
							<br/>
							<span className="grayTextColor">&#123;</span>
							<pre className="left-border">

"<span className="pinkTextColor">{`Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
do eiusmod tempor incididunt 
ut labor gravida.`}</span>"
<br/><br/>
"<span className="pinkTextColor">{`Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
do eiusmod tempor incididunt 
ut labor gravida.`}</span>"

							</pre>
							<span className="grayTextColor">&#125;</span>						
						</div>

						<div className="aboutMe__content-wrapper">
							<span className="grayTextColor about__title-fontSize2">&lt;&#47;</span>
							<span className="pinkTextColor about__title-fontSize2">Skills</span>
							<span className="grayTextColor about__title-fontSize2">&gt;</span>
							<br/><br/>
							<div className="left-border">

								<div className="barChart__main-wrapper">
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor" id="skillsHTML">
											HTML
										</div>
									</div>
									<span className="barChart-value" id="skillsHTML-value"></span>
								</div>	

								<div className="barChart__main-wrapper">
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor" id="skillsCSS">
											CSS
										</div>
									</div>
									<span className="barChart-value" id="skillsCSS-value"></span>
								</div>

								<div className="barChart__main-wrapper">
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor" id="skillsJS">
											JAVA SCRIPT
										</div>
									</div>
									<span className="barChart-value" id="skillsJS-value"></span>
								</div>

								<div className="barChart__main-wrapper">
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor" id="skillsREACT">
											REACT
										</div>
									</div>
									<span className="barChart-value" id="skillsREACT-value"></span>
								</div>

								<div className="barChart__main-wrapper" >
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor" id="skillsHTML1">
											HTML
										</div>
									</div>
									<span className="barChart-value" id="skillsHTML1-value"></span>
								</div>

								<div className="barChart__main-wrapper">
									<div className="barChart__inner-wrapper">
										<div className="barChart__bg-color whiteTextColor " id="skillsHTML2">
											HTML
										</div>
									</div>
									<span className="barChart-value" id="skillsHTML2-value"></span>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>	
		<img src={BGroundImg} className="BGround__image-On" alt="bg-on"/>
	</div>
	)
}


export default About;

	