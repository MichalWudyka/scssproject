import BGbGelipsa1 from '../../assets/BG-bGelipsa1.png'; 
import triangle from '../../assets/triangle.png'; 
import logo from '../../assets/logo.png'; 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faInstagram  } from '@fortawesome/free-brands-svg-icons';
 
const Footer = () => {
const instaIcon = <FontAwesomeIcon icon={faInstagram} />;
	return (

	<div className="footer__main-wrapper">
		<div className="footerGraphics__bGround-elipsa">
			<img src={BGbGelipsa1}  alt="BG-bGelipsa"/>
		</div>

		<div className="footer__inner-wrapper">
			<div className="footerGraphics__bGround-elipsa2">
				<img src={triangle} alt="triangle"/>
			</div>
			<div className="footer__content-wrapper">
				<div className="footerGraphics__icon-size footerGraphics__bgIcon-gray">f</div>
				<div className="footerGraphics__icon-size footerGraphics__bgIcon-pink">{instaIcon}</div>
				<div className="footerGraphics__icon-size footerGraphics__bgIcon-gray">in</div>
				<br/>
				<img src={logo} alt="LOGO" id="logo" className="logo" />
				<br/>
				MICHAŁ WUDYKA 2021
			</div>
			<hr className="footerGraphics__bot-line"/>
		</div>
	</div>
	)
}


export default Footer;

	